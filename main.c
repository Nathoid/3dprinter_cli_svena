/* dgteratreta

3D-Druckersteuerung

Semesterarbeit Juventus Fruejahrssemster 2024

 Autoren:
 Nathanael Gubler
 Svenja Ruoss

 git-repository:
 https://gitlab.com/Nathoid/3dprinter_cli_svena.git

 Funktion:
 CLI zur Bedienung eines imaginären 3D-Druckers.
*/

#include <stdio.h>
#include <dirent.h>
#include <time.h>
#include <string.h>

#define CODE_SEARCHFILES 1
#define CODE_PREHEAT 2
#define CODE_FILAMENTCHANGE 3
#define CODE_PRINT 4
#define PROGRAM_END 5

int file_read_successful = 0;
char file_name;

void show_init_prompt();
void show_menu();
int read_user_action();
float preheat(float current, float target, float step);
float step_temp(float current, float target, float step);
void wait (double sec);
float cool_down(float current1, float target1, float step);
char * show_files_in_USB();
int read_file(int file_ID, char (*paths_gdata)[]);

int main() {
//    Initialisierung
    show_init_prompt();
    int user_action = 0;
    float current_temp = 25.0;
    float target_temp;
    float current_time = 0;
    float target_time;
    int OK;
    int file_selection = 0;
    char * filepaths_gdata;

    while (1) {
        do {
            show_menu();
            user_action = read_user_action();
        } while (user_action == 0);
        switch (user_action) {
            case CODE_SEARCHFILES:
                printf("\n+++++++++++++++++++++FILES SUCHEN+++++++++++++++++++++\n");
                filepaths_gdata = show_files_in_USB();
                printf("Waehlen sie ihr gewuenschtes File\n");
                scanf("%d", &file_selection);
                read_file(file_selection, filepaths_gdata);

            case CODE_PREHEAT:
                printf("\n+++++++++++++++++++++VORHEITZEN+++++++++++++++++++++\n");
                printf("Drucker auf Temperatur voreizen\n");
                printf("Bitte Zieltemperatur eingeben:\n");
                if (scanf("%f", &target_temp) == 0) {
                    printf("ungueltige Eingabe");
                    break;
                }
                current_temp = preheat(current_temp, target_temp, 1.0);
                break;

            case CODE_FILAMENTCHANGE:
                printf("\n+++++++++++++++++++++FILAMENT WECHSEL+++++++++++++++++++++\n");
                printf("Filamentwechsel\n");
                printf("Bitte oeffne den Deckel\n");
                printf("Wenn offen 1 eingeben\n");
                if (scanf("%d", &OK) == 1) {
                    target_temp = 240;
                    current_temp = preheat(current_temp, target_temp, 5.0);
                }
                if (current_temp == 240) {
                    printf("\nSpule dreht gegen Uhrzeigersinn\n");
                    printf("Dies kann 3 bis 5 sek. dauern\n");
                    //target_time = 500;
                }
                //current_time = time(current_time, target_time);
                wait(5);

                printf("\nFilament draussen\n");
                printf("\nDie Spule kann nun entfernt werden\n");
                printf("Legen sie die neue Spule ein\n");
                printf("\nWenn neue Spule eingefuegt bestaetige mit 1\n");
                if (scanf("%d", &OK) == 1) {
                    printf("Spule dreht im Uhrzeigersinn\n");
                    printf("Kopf wird abgekuehlt\n");
                    target_temp = 25;

                    current_temp = cool_down(current_temp, target_temp, -5.0);
                    if (current_temp == 25) {
                        printf("Kopf ist nun abgekuehlt\n");
                        printf("\nDie Spule wurde ausgewechselt\n");
                        printf("Drucker wieder einsatzbereit\n");
                    }
                }
                break;

            case CODE_PRINT:
                printf("\n+++++++++++++++++++++DRUCKVORGANG+++++++++++++++++++++\n");
                printf("Druckvorgang\n");
                printf("Druckvorgang wird gestartet\n");

                current_temp = preheat(current_temp, target_temp, 5.0);
                printf("\nSpule dreht im Uhrzeigersinn\n");
                break;

            case PROGRAM_END:
                printf("Programm wird beendet.");
                return 0;
        }

    }
}
    void show_init_prompt() {
        /*Zeigt Infos beim ersten Start.*/
        printf("-------------------\nWillkommen zur 3D-Steuerung.\n-------------------\n\n");
        printf("Aktuelle Version: 0.1\n\n");
    }

    void show_menu() {
        /*Zeigt dem Benutzer das Auswahlmenu*/
        printf("=========================\nWas moechten sie tun?\n=========================\n");
        printf("Aktionen:\n");
        printf("%d: FILES SUCHEN\n", CODE_SEARCHFILES);
        printf("%d: VORHEITZEN\n", CODE_PREHEAT);
        printf("%d: FILAMENT WECHSEL\n", CODE_FILAMENTCHANGE);
        if (file_read_successful)
            printf("%d: DRUCK START (%c)\n", file_name, CODE_PRINT);
        printf("%d: Programm beenden\n", PROGRAM_END);

    }

    int read_user_action() {
        /*Liest eine Benutzereingabe ein und überprüft diese.*/
        int selection_success = -1;
        int user_input = -1;

        printf("\n=========================\nBitte zur Auswahl die jeweilige Nummer eingeben\n");
        selection_success = scanf("%d", &user_input);
        fflush(stdin);
        if ((selection_success == 0) || (user_input > PROGRAM_END)) {
            printf("Ungueltige Eingabe. Bitte %d bis %d eingeben. (Eingabe: %c)", CODE_SEARCHFILES, PROGRAM_END,
                   user_input);
            return 0;
        }
        return user_input;
    }

    float step_temp(float cur, float tar, float step) {
        // Prueft ob die Zieltemperatur schon erreicht ist. Wenn nicht, wird die Temperatur schrittweise verändert.
        if (((cur > tar) && (step > 0.0)) || ((cur < tar) && (step < 0.0))) {
            printf("Zieltemperatur bereits erreicht.");
            return cur;
        }
        if (step < 0.0) {
            while (cur > tar) {
                printf("aktuelle Temperatur: %.3f\n", cur);
                cur += step;
            }
        } else {
            while (cur < tar) {
                printf("aktuelle Temperatur: %.3f\n", cur);
                cur += step;
            }
        }
        printf("Zieltemperatur erreicht.");
        return cur;
    }

    float preheat(float current, float target, float temp_step) {
        // Prueft ob die Zieltemperatur schon erreicht ist. Wenn nicht, wird die Temperatur schrittweise erhoeht.
        current = step_temp(current, target, temp_step);
        printf("Vorheizen abgeschlossen bei Temperatur %.3f\n", current);
        return current;
    }

    void wait (double sec){
        double diff = 0;

        clock_t t1 = clock();

        while(diff < sec ){
            clock_t t2 = clock();
            diff = (t2 - t1)/ CLOCKS_PER_SEC;
        }
        //return;
    }

    float cool_down(float current1, float target1, float step) {
        // Prueft ob die Zieltemperatur schon erreicht ist. Wenn nicht, wird die Temperatur schrittweise erhoeht.

        current1 = step_temp(current1, target1, step);
        printf("Abkuehlen abgeschlossen %.3f\n", current1);
        return current1;
    }

    char * show_files_in_USB() {
        // Liest den Inhalt des USB-Sticks aus. Eine Liste von Dateipfaden zurück
        struct dirent *de;  // Pointer for directory entry
        int numb_files = 0;
        char dot = '.';
        char dotg[] = ".g";
        char filepaths[256][256];

        // opendir() returns a pointer of DIR type.
        DIR *dr = opendir("..\\USB_dir");

        if (dr == NULL)  // opendir returns NULL if couldn't open directory
        {
            printf("Das USB-Directory konnte nicht geoeffnet werden");
            return 0;
        }

        printf("Inhalt des USB-Directory:\n");

        while ((de = readdir(dr)) != NULL){
            numb_files++;
            if((de->d_name[0] != dot) && (strstr(de->d_name,dotg) != NULL)){
                printf("File Nr.%d:\t%s\n",numb_files, de->d_name);
                strcat(filepaths[numb_files],"..\\USB_dir");
                strcat(filepaths[numb_files],de->d_name);
                }
        }
        closedir(dr);
        return *filepaths;
    }

    int read_file(int file_ID, char (*paths_gdata)[256]){
        // Liest den angegebenen Pfad aus und wertet dessen Inhalt aus.
        // ************ Diese übergabe funktioniert anscheinend nicht richtig ************
        struct dirent *de;  // Pointer for directory entry
        int file_counter = 0;
        FILE *fp;
        char time_temp[256];

        printf("File Nr.%d wird ausgelesen.\n",file_counter);
        fp = fopen(paths_gdata[file_ID],"r");
        if (fp == NULL){
            printf("Das gewählte File konnte nicht geoeffnet werden");
            return 0;
        }
        printf("Fileinhalt wird gelesen...");
        fgets(time_temp,sizeof time_temp, fp);
        printf("%s",time_temp);

        fclose(fp);
        return 1;
}
